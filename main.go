// main.go

package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

type api struct {
	db *sql.DB
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "HealthCheck: Service is up!")
}

// Sum computes sum of two integers
func Sum(x int, y int) int {
	return x + y
}

func main() {
	// Establish DB connection:
	// Update the .env file with DATABASE_URL in .env file, then source .env file
	// To run the app, run $ go build to build the binary and then $ ./go-rest-mux-app
	// to run the app.
	// To run the app within docker, following command runs the app on
	// localhost port 5000. It uses the --env flags to inject the database
	// variables. "host.docker.internal" is used instead of db server IP if your
	// database is also running within docker.
	// $ docker run -d -p 5000:5000 --env DATABASE_URL='postgres://user:password@host.docker.internal:5432/postgres' registry.gitlab.com/mayankkapoor/go-rest-mux-app:latest
	databaseURL := getEnv("DATABASE_URL", "postgres://postgres:password@127.0.0.1:5432/postgres")
	dbURLarray := strings.FieldsFunc(databaseURL, func(r rune) bool {
		if r == ':' {
			return true
		}
		if r == '@' {
			return true
		}
		if r == '/' {
			return true
		}
		return false
	})

	databaseHost := dbURLarray[3]
	databasePort, err := strconv.Atoi(dbURLarray[4])
	if err != nil {
		log.Error(err)
	}
	databaseUser := dbURLarray[1]
	databasePass := dbURLarray[2]
	databaseName := dbURLarray[5]

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", databaseHost, databasePort, databaseUser, databasePass, databaseName)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Error(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Error(err)
	}
	fmt.Printf("Connected with database on host %v & port %v\n", databaseHost, databasePort)

	// Initialize tables needed for app
	sqlStatement := `
	CREATE TABLE posts (
		id SERIAL PRIMARY KEY,
		title TEXT NOT NULL
	)`
	_, err = db.Exec(sqlStatement)
	if err != nil {
		log.Warn(err)
	}

	app := &api{db: db}
	router := NewRouter(app)

	servicePort := getEnv("SERVICE_PORT", "5000")
	servicePortText := fmt.Sprintf("%s%s", ":", servicePort)

	fmt.Printf("Your REST API server is listening on port %v\n", servicePort)
	http.ListenAndServe(servicePortText, router)
}
