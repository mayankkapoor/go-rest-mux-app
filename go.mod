module gitlab.com/mayankkapoor/go-rest-mux-app

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/golang/mock v1.4.3
	github.com/gorilla/mux v1.7.4
	github.com/lib/pq v1.3.0
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/objx v0.2.0 // indirect
)
