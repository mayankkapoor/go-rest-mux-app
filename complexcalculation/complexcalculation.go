// complexcalculation.go makes a complex calculation using go routines & channels.

package complexcalculation

func calcSquares(number int, squareop chan int) {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit
		number /= 10
	}
	squareop <- sum
}

func calcCubes(number int, cubeop chan int) {
	sum := 0
	for number != 0 {
		digit := number % 10
		sum += digit * digit * digit
		number /= 10
	}
	cubeop <- sum
}

// ComplexCalculation calculates the complex sum of the integer
// E.g. ComplexCalculation(123) =
// squares = (1 * 1) + (2 * 2) + (3 * 3)
// cubes = (1 * 1 * 1) + (2 * 2 * 2) + (3 * 3 * 3)
// answer = squares + cubes = 50
func ComplexCalculation(number int, squareop chan int, cubeop chan int) int {
	go calcSquares(number, squareop)
	go calcCubes(number, cubeop)
	squares, cubes := <-squareop, <-cubeop
	return squares + cubes
}
