// complexcalculation_test.go tests the functions in carfactory

package complexcalculation

import "testing"

func TestCalcSquares(t *testing.T) {
	data := 123
	expected := 14
	squarech := make(chan int)
	go calcSquares(data, squarech)
	actual := <-squarech
	if actual != expected {
		t.Errorf("Calculated Squared Sum was incorrect, got: %d, expected: %d.", actual, expected)
	}
}

func TestCalcCubes(t *testing.T) {
	data := 123
	expected := 36
	cubech := make(chan int)
	go calcCubes(data, cubech)
	actual := <-cubech
	if actual != expected {
		t.Errorf("Calculated Cubed Sum was incorrect, got: %d, expected: %d.", actual, expected)
	}
}

func TestComplexCalculation(t *testing.T) {
	data := 123
	expected := 50
	squarech := make(chan int)
	cubech := make(chan int)
	// This breaks apart the complex calculation into two parallel sub calculations
	actual := ComplexCalculation(data, squarech, cubech)
	if actual != expected {
		t.Errorf("Calculated Complex Sum was incorrect, got: %d, expected: %d.", actual, expected)
	}
}
